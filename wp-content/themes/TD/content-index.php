<section id="content-wrapper" data-content-field="main-content">
      
        <!-- Section TD -->

        <div class="parallax-item" >    
        
          <div id="terrerium-design-small" class="visible-sm visible-xs">
            <section class="module parallax parallax-1">
			
				<div class="container">
					<h1 class="title" class="page-title" >Terr<span class = "a-name">a</span>rium <br/> Design</h1>
				</div>
			</section>

            
          </div>
		
          <div id="terrerium-design" class="hidden-sm hidden-xs" data-color-suggested="#dedfd9">

              <div style="text-align:center; top: 50%; left: 50%; transform: translateX(-50%) translateY(-50%);" class="title-desc-inner">
                <div class="page-title-wrapper"><h1 style="letter-spacing: 0.08em;" class="page-title" >Terr<span class = "a-name">a</span>rium <br/> Design</h1></div>                <br> <!-- this keeps the inline-blocks from collapsing -->
                <div class="page-desc" data-content-field="description"></div>
              </div>
    
          </div>
    
          <div class="content has-main-image">
            <div class="content-inner content-inner-about has-content" data-content-field="main-content">
                
                <div class="sqs-layout sqs-grid-12 columns-12">
                  <div class="row sqs-row">
                    <div class="col sqs-col-12 span-12">
                      <div class="sqs-block html-block sqs-block-html" >
                        <div class="sqs-block-content" id = "philosophy">
                          <h1 class="text-align-center">PHILOSOPHY</h1>
                          <p class="text-align-center about-text">'Terrarium' is a living green ecosystem enclosed in a glass container, often used as an ornamental piece that creates an unique environment for tropical plant growth.
                            Terrarium Design is based on the same promise of creating unique, elegant home spaces that are personal and bring out one's identity. Our philosophy is to create homes that bring out distinctive style and personality. Elegance and fine design come with our signature in spaces that have an instant emotional connect.
                            We believe in ' Less is Elegant, More is Decorous ' based on individual taste and style.
                          </p>
                        </div>
                      </div>


                      <div class="sqs-block html-block sqs-block-html" >
                        <div class="sqs-block-content" id = "who-we-are">
                          <h1 class="text-align-center">WHO WE ARE</h1>
                          <p class="text-align-center about-text">Terrarium Design is a team of like minded professionals with diverse talents and ideas that collaborate together to bring out great design. We are a full-service interior design firm dedicated to transforming still spaces into beautiful yet personal and functional environments. It’s our passion to deliver personalized and performance driven interior design solutions to our clients to help bring the space to life !
                            Terrarium Design was founded to create unique Design spaces that are aesthetic and functional. 
                          </p>
                        </div>
                      </div>

                      <div class="sqs-block html-block sqs-block-html" >
                        <div class="sqs-block-content">
                          <h1 class="text-align-center" id = "what-we-do">WHAT WE DO</h1>
                          <p class="text-align-center about-text">
                              <strong>Residential</strong>
                              <br>
                              Residential interiors for Bungalows, flats and farmhouses
                              <br>
                              <strong>Commercial</strong>
                              <br>
                              Showrooms, cafes, restaurants
                              <br>
                              <strong>Work place</strong>
                              <br>
                              Offices 
                          </p>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
            </div>
          </div>
        </div>

        <!-- Section Our Approach -->
  
        <div class="parallax-item" data-url-id="the-team">
          <!-- Show this in small screen devices -->
          <div id="terrerium-approach-small" class="visible-sm visible-xs">
		  <div id = "our-approach" class = "our-approach"> </div>
            <section class="module parallax parallax-2">
				<div class="container">
					<h1 class="title" class="page-title" >Our Approach</h1>
				</div>
			</section>
          </div>

		  <!-- Hide this in small screen devices -->
          <div id="terrerium-approach"  style="padding-top: 154px;" class="hidden-sm hidden-xs" data-color-suggested="#c7c0b8">

            <div id = "our-approach"> </div>

              <div  style="text-align:center; top: 50%; left: 50%; transform: translateX(-50%) translateY(-50%);" class="title-desc-inner"  data-edit-main-image="Background">
                <div class="page-title-wrapper"><h1 style="letter-spacing: 0.08em;" data-shrink-original-size="100" class="page-title" data-content-field="title">Our Approach</h1></div>
                <br> <!-- this keeps the inline-blocks from collapsing -->
                <div class="page-desc" data-content-field="description"></div>
              </div>
          </div>

          <div  class="content has-main-image">
            <div class="content-inner approach-content-inner has-content" data-content-field="main-content">                     
                
              <section class="">
                <div class="">

                  <div class="">

                    <div class="wpb_column vc_column_container vc_col-sm-12">
                      <div class="vc_column-inner ">
                        <div class="wpb_wrapper ">
                          <div class="de_tab tab_steps">
        
                            <ul class="de_nav">
                              <li style="visibility: visible; animation-delay: 0s; animation-name: fadeIn;" class="wow fadeIn animated" data-wow-delay="0s">
                                  <span class = "active" id = "1">Collaborate</span>
                                  <div style="opacity: 0;" class="v-border"></div>
                              </li>
            
                              <li style="visibility: visible; animation-delay: 0.4s; animation-name: fadeIn; opacity: 1;" class="wow fadeIn animated" data-wow-delay="0.4s">
                                <span id = "2">Ideate  &amp; Inspire</span>
                                <div style="opacity: 0;" class="v-border"></div>
                              </li>
            
                              <li style="visibility: visible; animation-delay: 0.8s; animation-name: fadeIn; opacity: 1;" class="wow fadeIn animated" data-wow-delay="0.8s">
                                <span id = "3">Design Creation </span>
                                <div style="opacity: 0;" class="v-border"></div>
                              </li>
            
                              <li style="visibility: visible; animation-delay: 1.2s; animation-name: fadeIn; opacity: 1;" class="wow fadeIn animated" data-wow-delay="1.2s">
                                <span id = "4">Develop &amp; Construct</span>
                                <div style="opacity: 0;" class="v-border"></div>
                              </li>  

                              <li style="visibility: visible; animation-delay: 1.2s; animation-name: fadeIn; opacity: 1;" class="wow fadeIn animated" data-wow-delay="1.2s">
                                <span id = "5">Execute &amp; Setup</span>
                                <div style="opacity: 0;" class="v-border"></div>
                              </li>

                            </ul>

                            <div class="de_tab_content">
        
                              <div style="display: block;" id="tab1">
                                <p>
                                  Understanding of client brief through collaborative client + designer interview
                                </p>                                
                              </div>

                              <div style="display: none;" id="tab2">
                                <p>
                                  Presentation of client brief through design concept boards. Creation of a design story for your space. The big idea !                                </p>                                
                              </div>

                              <div style="display: none;" id="tab3">
                                <p>
                                  Based on the design concept, creating of technical design layouts, material boards. Design presentation with specifics
                                </p>                                
                              </div>

                              <div style="display: none;" id="tab4">
                                <p>
                                  Developing the final design concept into actuals. Collaborating with vendors and fabricators
                                </p>                                
                              </div>

                              <div style="display: none;" id="tab5">
                                <p>Onsite execution of design concept and final set up
                                </p>                                
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>

            </div>
          </div>
        </div>



        <!-- Section Team -->
  
        <div class="parallax-item" data-url-id="the-team">
          
          <div id="team-terrerium-small" class="visible-sm visible-xs">
            <div id = "our-team" class = "our-team"> </div>
            <section class="module parallax parallax-3">
				<div class="container">
					<h1 class="title" class="page-title" >Te<span class = "a-name">a</span>m Terrarium</h1>
				</div>
			</section>
          </div>

          <div id="team-terrerium" style="padding-top: 154px;" class="hidden-sm hidden-xs" data-color-suggested="#c7c0b8">

              <div id = "our-team"> </div>
   
              <div style="text-align:center; top: 50%; left: 50%; transform: translateX(-50%) translateY(-50%);" class="title-desc-inner">
                <div class="page-title-wrapper"><h1 style="letter-spacing: 0.08em;" data-shrink-original-size="100" class="page-title" data-content-field="title">Team Terr<span class = "a-name">a</span>rium</h1></div>
                <br> <!-- this keeps the inline-blocks from collapsing -->
                <div class="page-desc" data-content-field="description"></div>
              </div>
          </div>

          <div  class="content has-main-image">
            <div class="content-inner content-inner-team has-content" data-content-field="main-content">                     
                
                <div class="row sqs-row" >
                  <div class="col-sm-3 team-person-image-wrapper">
                    <img class = "team-person-image img-responsive" src = "http://terrariumdesign.in/TD/wp-content/uploads/2016/06/geetanjali.jpg" height = "150" width = "100%">
                  </div>

                  <div class="col-sm-9">
                      <div class = "team-person-name">Gitanjali Nagarkatti</div>
                      <div class = "team-person-post">Principal Founder</div>
                      <div class = "team-person-description">
                        After Post-Grad Diploma in Textile Design from, National Institute of Design, Ahmedabad, Gitanjali pursued her career in the domestic retail industry handling leading brands in Design and product management. The creative pursuits extended in her passion for home design that kept Gitanjali updated with home design trends and styles. Keen eye for detail and finesse helped in raising the bar of design aesthetics. Gitanjali believes in a process driven,  professional approach to design that helps in keeping the clients updated at every stage of the project
                      </div>
                  </div>
                </div>

                <br/><hr/><br/>

                <div class="row sqs-row">
                  <div class="col-sm-3 team-person-image-wrapper">
                    <img class = "team-person-image img-responsive" src = "<?php echo get_stylesheet_directory_uri() ?>/img/Bharati.JPG" height = "150" width = "100%">
                  </div>

                  <div class="col-sm-9">
                      <div class = "team-person-name">Bharati Choubey </div>
                      <div class = "team-person-post">Principal Founder</div>
                      <div class = "team-person-description">
                        A Management Graduate from  SIBM, Bharati has expertise in managing projects and planning for execution. Her keen eye for detail ensures seamless implementation of backend processes without compromising on quality. Her utilitarian inputs regarding projects ensure utmost customer satisfaction.  
                      </div>
                  </div>
                </div>

                <br/><hr/><br/>

                <div class="row sqs-row">
                  <div class="col-sm-3 team-person-image-wrapper">
                    <img class = "team-person-image img-responsive" src = "<?php echo get_stylesheet_directory_uri() ?>/img/Pallavi.PNG" height = "150" width = "100%">
                  </div>

                  <div class="col-sm-9">
                      <div class = "team-person-name">Pallavi Neve </div>
                      <div class = "team-person-post">Principal Architect</div>
                      <div class = "team-person-description">
                        Pallavi brings more then 20 years of space design experience, Being an Architect, her work is a constant pursuit of a visionary aesthetic that encompasses all fields of design. She believes that once the trust of a client and the concept plan is resolved everything else, falls in place. She is always driven to constantly reinvent her work. Her central concerns involve a simultaneous engagement in practice and research.
                      </div>
                  </div>
                </div>

                <br/><hr/><br/>

                <div class="row sqs-row">
                  <div class="col-sm-3 team-person-image-wrapper">
                    <img class = "team-person-image img-responsive" src = "<?php echo get_stylesheet_directory_uri() ?>/img/Ashwairya.PNG" height = "150" width = "100%">
                  </div>

                  <div class="col-sm-9">
                      <div class = "team-person-name">Aishwarya Khandelwal</div>
                      <div class = "team-person-post">Principal Interior Designer</div>
                      <div class = "team-person-description">
                        A  qualified Interior designer , Aishwarya aims at transforming every project into a stimulating environment, rich in inventive detailing. Her dynamic nature and sparkling persona reflects in her work, leaving an everlasting impression on the minds of spectators.

                        She  utilizes her expertise of Interior elements seamlessly  while anticipating & overcoming hurdles leading to successful completion of Projects, she brings 15 years of experience in interior design and execution 

                      </div>
                  </div>
                </div>

            </div>
          </div>
        </div>






        <!-- Section Our Work -->

  
        <div class="parallax-item" data-url-id="the-team">
          
          <div id="terrerium-work-small" class="visible-sm visible-xs">
            <div id = "our-work" class = "our-work"> </div>
            <section class="module parallax parallax-4">
				<div class="container">
					<h1 class="title" class="page-title" >Our Work</h1>
				</div>
			</section>
          </div>

          <div id="terrerium-work" style="padding-top: 154px;" class="hidden-sm hidden-xs" data-color-suggested="#c7c0b8">

            <div id = "our-work"> </div>
   
              <div style="text-align:center; top: 50%; left: 50%; transform: translateX(-50%) translateY(-50%);" class="title-desc-inner" >
                <div class="page-title-wrapper"><h1 style="letter-spacing: 0.08em;" class="page-title" >Our Work</h1></div>
                <br> <!-- this keeps the inline-blocks from collapsing -->
                <div class="page-desc" data-content-field="description"></div>
              </div>
          </div>

          <div  class="content has-main-image">
            <div class="content-inner content-inner-work has-content" data-content-field="main-content">                     
                <div>  

                    <?php echo do_shortcode('[display-td-gallery]'); ?>

                </div> 

                <div class = "view-more-gallery">
                  <a href = "<?php echo site_url() ?>/gallery/"><button type="button" class="btn btn-default">View Gallery</button></a>                
                </div> 

            </div>
          </div>
        </div>


            <div class="back-to-top"></div>
        </section>

        