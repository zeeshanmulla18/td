<?php 

	// Public JS scripts
	if (!function_exists('dct_scripts_method')) {
		function dct_scripts_method() {		
			// JS
			wp_enqueue_script('myscriptjs', get_stylesheet_directory_uri() . '/js/my-sctipt.js', array('jquery'), '', TRUE);
			wp_enqueue_script('demojs', get_stylesheet_directory_uri() . '/js/demo.js', array('jquery'), '', TRUE);
		}
	}
	add_action('wp_enqueue_scripts', 'dct_scripts_method');

	// Public CSS files
	if (!function_exists('dct_style_method')) {
		function dct_style_method() {		
			// Style
			wp_enqueue_style( 'site-css', get_stylesheet_directory_uri() . '/css/site.css', array()); 
		}
	}
	add_action('wp_enqueue_scripts', 'dct_style_method');
	add_action('wp_head', 'dct_style_method');



include('inc/index-page.php');


// Registering the menu
	register_nav_menu( 'inner_menu', 'Inner Pages Menu' );
