/**
 * Main Javascript.
 * This file is for who want to make this theme as a new parent theme and you are ready to code your js here.
 */

 //////////////////jquery smooth scroll to id's
  jQuery('.nav a ').click(function() {
    // event.preventDefault();
    jQuery('.nav li').removeClass("current-custom-menu-item");
    jQuery(this).closest("li").addClass("current-custom-menu-item");


    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = jQuery(this.hash);
      console.log(target);
      target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
      console.log(target);
      if (target.length) {
        jQuery('html,body').animate({
          scrollTop: target.offset().top-110
        }, 1000);
        return false;
      }
    }
  });

//////////////////////// Back to top
  jQuery(document).ready(function($){ 
    jQuery(window).scroll(function(){ 
        if (jQuery(this).scrollTop() > 100) { 
            jQuery('#scroll').fadeIn(); 
        } else { 
            jQuery('#scroll').fadeOut(); 
        }
    }); 
    jQuery('#scroll').click(function(){ 
        jQuery("html, body").animate({ scrollTop: 0 }, 600); 
        return false; 
    }); 


    /* Height of the bg image */

    // var hgt = $(window).height();
    // $("#eight").css("height", hgt);

    /* our Approach Section */

    var width = $(window).width();
    if(width < 1160){ 
      $('.v-border').css('opacity',0);
    }


    /* Add and remove id */

    if(width > 600){ 
      $('.our-approach').attr('id','');
      $('.our-team').attr('id','');
      $('.our-work').attr('id','');
    }else {
      $('.our-approach').attr('id','our-approach');
      $('.our-team').attr('id','our-team');
      $('.our-work').attr('id','our-work');
    }


    $('.de_nav li span').click(function(){

      var idd = $(this).attr('id');
      var id_attr = 'tab'+idd;

      $('.de_nav li span').removeClass('active');
      // $('.v-border').css('opacity',0);
      $(this).addClass('active');
      if(width > 1160){ 
        // $(this).next('.v-border').css('opacity',1);
      }

      $('.de_tab_content div').hide();
      $("#"+id_attr).show();


      // In small screen scroll to a certain div
      
      

      if(width < 770){
        var $container = $("html,body");
        var $scrollTo = $('.de_tab_content');
        $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top-50, scrollLeft: 0},300);
      }

    })


    /* Close the Nav on click */

    $('.menu-item a').click(function(){
      $('#menu-item-76').removeClass('open');
      $('.navbar-toggle').click();
    });
	
    $('#menu-item-76').addClass('current-custom-menu-item');
  
	(function(){

  var parallax = document.querySelectorAll(".parallax"),
      speed = 0.5;

  window.onscroll = function(){
    [].slice.call(parallax).forEach(function(el,i){

      var windowYOffset = window.pageYOffset,
          elBackgrounPos = "0 " + (windowYOffset * speed) + "px";
      
      el.style.backgroundPosition = elBackgrounPos;

    });
  };

})();
	
});

