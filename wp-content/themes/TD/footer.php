<?php
/**
 * The theme footer
 * 
 * @package bootstrap-basic
 */
?>

      </div><!--.site-content-->
      </div>
      
      <div id="footerr" id = "contact-uss" class= "row">
            <div class = "col-sm-6 ">
                <div class = "row get-in-touch" id = "contact-us">

                    <div class = "contact-us-title"> Get in Touch </div>
                    <br/><br/><br/>
                    <i class="fa fa-mobile" style="font-size:26px"></i> +91-9167853580 , +91-9822083296
                    <br/><br/>
                    <i class="fa fa-briefcase" style="font-size:26px"></i> Terrarium Design Pvt. Ltd., Pune
                    <br/><br/>
                    <i class="fa fa-envelope" style="font-size:26px"></i> <a class = "mailto-link" href="mailto:info@terrariumdesign.in" style = "color:#000000" target="_top">info@terrariumdesign.in</a>

                </div>
              </div>

              <div class = "col-sm-6 contact-us">
                <?php echo do_shortcode('[contact-form-7 id="57" title="Contact form 1"]'); ?>
              </div>
      </div>

      
    </div><!--.container page-container-->
    <!-- BackToTop Button -->
<a href="javascript:void(0);" id="scroll" title="Scroll to Top" style="display: none;"><span></span></a>
    
    
    <!--wordpress footer-->
    <?php wp_footer(); ?> 
  </body>
</html>