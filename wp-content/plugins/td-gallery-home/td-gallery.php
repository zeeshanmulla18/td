<?php 
/**
 *
 * Plugin Name:       Add Gallery Images
 * Description:       Add Gallery Images from here
 * Version:           0.0.1
 * Author:           Zeeshan
 */


    
    /*----------------------------------------------------
                        Gallery Images
    -----------------------------------------------------*/


    /* Custom Post types Add Events */


    add_action( 'init', 'create_custom_post_type_images' );

    function create_custom_post_type_images(){
        
        /* Pricing */

        register_post_type( 'td_gallery',
            array(
                'labels' => array(
                    'name' => 'All Images',
                    'singular_name' => 'Images',
                    'add_new' => 'Add Images',
                    'add_new_item' => 'Add New Images',
                    'edit' => 'Edit',
                    'edit_item' => 'Edit Images',
                    'new_item' => 'New Images',
                    'view' => 'View',
                    'view_item' => 'View Images',
                    'search_items' => 'Search Images',
                    'not_found' => 'No Images found'
                ),
    
                'public' => true,
                'menu_position' => 100,
                'supports' => array( 'title','thumbnail'),
                'taxonomies'  => array( 'category' ),
                'has_archive' => true
            )
        );


        add_action( 'wp_enqueue_scripts', 'wp_enqueue_scripts_custom_pricing' );
        
        function wp_enqueue_scripts_custom_pricing() {

            wp_enqueue_style( 'slick', plugins_url('css/slick.css', __FILE__ ), array()); 
            wp_enqueue_style( 'slicktheme', plugins_url('css/slick-theme.css', __FILE__ ), array()); 
            wp_enqueue_style( 'font-awesome', plugins_url('css/font-awesome.min.css', __FILE__ ), array()); 
            wp_enqueue_style( 'custom-filterable', plugins_url('css/custom.css', __FILE__ ), array()); 

            wp_enqueue_script( 'slickjs', plugins_url('js/slick.min.js', __FILE__ ),array('jquery') );
            // wp_enqueue_script( 'shufflejs', plugins_url('js/jquery.shuffle.min.js', __FILE__ ),array('jquery') );
            wp_enqueue_script( 'custom-scripts-easing', plugins_url('js/jquery.easing.min.js', __FILE__ ),array('jquery') );
            wp_enqueue_script( 'custom-scripts-mixitup', plugins_url('js/jquery.mixitup.min.js', __FILE__ ),array('jquery') );
            wp_enqueue_script( 'custom-scripts', plugins_url('js/custom.js', __FILE__ ),array('jquery') );

         }

    }




    /* Display Front End */

    function display_td_gallery_function() {
        ob_start();
        display_td_gallery();
        $output_string = ob_get_contents();
        ob_end_clean();
        return $output_string;  
    }


    function display_td_gallery() {

    // Views Arguments
    $args_views = array(
        'post_type'      => 'td_gallery',
        'post_status' => 'publish',
    'posts_per_page' => 5,
    'category_name' => 'home'
    );
    
    $posts_views = new WP_Query( $args_views ); ?>
        
        <div class = "home-image-gallery">
            
            <?php 
                if ( $posts_views->have_posts() ) {
        
                    while ( $posts_views->have_posts() ) {
                        $posts_views->the_post();
                        $id = get_the_id();
                        $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($id), 'full' );         
                        $image_url = $thumb[0];
            ?>

                <div>
                    <a ><img class = "img home-gallery-img img-responsive" src="<?php echo $image_url; ?>" height = "250"/></a>
                </div>

            <?php 
                    }
                    wp_reset_postdata();
                }
            ?>
            
        </div>


    <?php }
    add_shortcode( 'display-td-gallery', 'display_td_gallery_function');




    /* Filterable Grid */

    function my_func_shortcode_artwork($atts) {
        ob_start();
        artWork();

        $output_string = ob_get_contents();

        ob_end_clean();

        return $output_string;
    }


    function artWork() {

        $artwork_array = array(
            'post_type'      => 'td_gallery',
            'post_status' => 'publish',
            'numberposts' => 500,
            'orderby' => 'post_date',
            'ignore_sticky_posts' => '1',
            'category'=>'3,4,5', // term_id in wp_terms table for respective category
            'orderby' => 'rand',
        );
        $artwork = wp_get_recent_posts( $artwork_array );

    //var_dump($artwork);

        // Container in which all images are displayed
        echo '<div class="container">';
        
            // Static menu
            echo '<ul id="filters" class="clearfix">';
                echo '<li><span class="filter active" data-filter="residential commercial workplace">All</span></li>';
                echo '<li><span class="filter" data-filter="residential">Residential</span></li>';
                echo '<li><span class="filter" data-filter="commercial">Commercial</span></li>';
                echo '<li><span class="filter" data-filter="workplace">Workplace</span></li>';
                
            echo '</ul>';


            // Display of images
            echo '<div id="portfoliolist">';
            foreach( $artwork as $artwork_images ){


                // gets the category of the post
                $category = get_the_category($artwork_images['ID']);
                $category_name = $category[0]->name;

                $artWorkTags = wp_get_post_tags($artwork_images['ID']);
                $artWorktagName = $artWorkTags[0]->name;

                // Taking featured image and getting the image src
                $thumbnail_image = get_the_post_thumbnail($artwork_images['ID']);
                preg_match ('/src="(.*)" class/',$thumbnail_image,$link);                
                 
                echo '<div class="portfolio'.' '.$category_name.'" data-cat="fan">';
                    echo '<div class="portfolio-wrapper">';                
                        if($artWorktagName == "sale"){
                        echo '<div class = "saleClass">';
                        echo '<div class="ribbon-wrapper-green hiring"><div class="ribbon-green">SALE</div></div>';
                            ?>

                        <?php echo '<a href = "'.$link[1].'" title = "'.get_post_field('post_content', $artwork_images['ID']).'" rel = "lightbox">'.'<img src = "'.$link[1].'" height = "200">'.'</a>'; ?>

                            <?php
                            echo '<div class="label">';
                                echo '<div class="label-text">';
                                    echo '<a class="text-title">'.$artwork_images['post_title'].'</a>';
                                echo '</div>';
                                echo '<div class="label-bg"></div>';
        
                            echo '</div>';
                        echo '</div>';
                        }    
                        else {
                            echo '<div class = "normalClass">'; ?>

                        <?php echo '<a href = "'.$link[1].'" title = "'.get_post_field('post_content', $artwork_images['ID']).'" rel = "lightbox">'.'<img src = "'.$link[1].'" height = "200">'.'</a>'; ?>
                        <?php    echo '<div class="label">';
                                echo '<div class="label-text">';
                                    echo '<a class="text-title">'.$artwork_images['post_title'].'</a>';
                                echo '</div>';
                                echo '<div class="label-bg"></div>';
        
                            echo '</div>';
                        echo '</div>';

                        }
                        echo '</div>';
                    echo '</div>';            
            }
            echo '</div>';
        echo '</div>'; // container ends
    }
    add_shortcode('art-work','my_func_shortcode_artwork');
