jQuery(document).ready(function($){

  /* Slick Corousal */
  jQuery('.home-image-gallery').slick({
    slidesToShow: 3,
      slidesToScroll: 1,
      dots: true,
      autoplay: false,
      infinite: false,
      // autoplaySpeed: 6780,

      responsive: [{

      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        infinite: false
      }

    }, {

      breakpoint: 991,
      settings: {
        slidesToShow: 2,
        dots: true
      }

    }, {

      breakpoint: 678,
      settings: {
        slidesToShow: 1,
        dots: true
      }
    
    }]
  });

  
  /* Slick Corousal for Blog*/
  jQuery('.blog-slider').slick({
    slidesToShow: 3,
      slidesToScroll: 1,
      dots: true,
      autoplay: false,
      infinite: false,
      // autoplaySpeed: 6780,

      responsive: [{

      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        infinite: false
      }

    }, {

      breakpoint: 991,
      settings: {
        slidesToShow: 2,
        dots: true
      }

    }, {

      breakpoint: 678,
      settings: {
        slidesToShow: 1,
        dots: true
      }
    
    }]
  });



/* Slick Corousal for Blog*/
  jQuery('.event-workshop-wrapper').slick({
    slidesToShow: 3,
      slidesToScroll: 1,
      dots: true,
      autoplay: false,
      infinite: false,
      // autoplaySpeed: 6780,

      responsive: [{

      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        infinite: false
      }

    }, {

      breakpoint: 991,
      settings: {
        slidesToShow: 2,
        dots: true
      }

    }, {

      breakpoint: 678,
      settings: {
        slidesToShow: 1,
        dots: true
      }
    
    }]
  });
})

/* Filterable Grid */
jQuery(function () {
        
        var filterList = {
        
            init: function () {
            
                // MixItUp plugin
                // http://mixitup.io
                jQuery('#portfoliolist').mixitup({
                    targetSelector: '.portfolio',
                    filterSelector: '.filter',
                    effects: ['fade'],
                    easing: 'snap',
                    // call the hover effect
                    onMixEnd: filterList.hoverEffect()
                });             
            
            },
            
            hoverEffect: function () {
            
                // Simple parallax effect
                // jQuery('#portfoliolist .portfolio').hover(
                //     function () {
                //         jQuery(this).find('.label').stop().animate({bottom: 0}, 200, 'easeOutQuad');
                //         jQuery(this).find('img').stop().animate({top: -30}, 500, 'easeOutQuad');                
                //     },
                //     function () {
                //         jQuery(this).find('.label').stop().animate({bottom: -40}, 200, 'easeInQuad');
                //         jQuery(this).find('img').stop().animate({top: 0}, 300, 'easeOutQuad');                              
                //     }       
                // );              
            
            }

        };
        
        // Run the show!
        filterList.init();
        
        jQuery('#overlay').hover(function() {
            alert("h");
        });

        jQuery("#lightbox iframe").on("hover",function(){ 
            //alert("h");
        });

        
        
    }); 