<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'td');
define('DB_USER', 'root');
define('DB_PASSWORD', '');
define('DB_HOST', 'localhost');

/* Server */
// define('DB_NAME', 'terrarium_db');
// define('DB_USER', 'terrarium_user');
// define('DB_PASSWORD', 'O{C-)~cch_oQ');
// define('DB_HOST', 'localhost');


// define('WP_HOME','http://192.168.43.126/TD/terrarium');
// define('WP_SITEURL','http://192.168.43.126/TD/terrarium');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'enC$3F_epih*&(0t6{ZA-V2;6l]D/<Tl:WEnOGLpK&{Ry%cL_BKSW;cs,R +F#+[');
define('SECURE_AUTH_KEY',  's*0^sLnLz~mABO*|Nw>,mN9.,gFR{_azzCKvQGsIV;:MN0egt~aAqw^%|kT5ko6m');
define('LOGGED_IN_KEY',    'v&?0 HWY.7oa{bbrh6QDMc7wlBU,}SCu_Vy|,I~2PD[?|PgTPj<xl`}R5dL:Q%t[');
define('NONCE_KEY',        '$<qZ41kOU#TmAx=#@Ym,ru03uR0wE_]c@F;3?FFW$0|f zC=;L*)^Lg9jVVw>i@!');
define('AUTH_SALT',        ':V]r?z6{8@ns/CD57I2sRdh@n|E^cUl.$SQ<C^E>|`VfJZ?Vx`ac2g *CfG?QaBa');
define('SECURE_AUTH_SALT', '7^G6!I1^G#1wEACp)Ue;bCInAS&2;T]^03$U%8P#`+*;y/^>_`fBleO4},ENX->M');
define('LOGGED_IN_SALT',   'UD.dfd*MR#5 dhplNYi)/]CT{*=cbPPkHghLBFd}?;@0IOBhc)v/F<R2T~C_M%Rv');
define('NONCE_SALT',       ',>*$YG3{D1DEp5rZ(CF|9bv^`g6Q2`]Cb/eYXmz~Z2y:Vg]g/WpUBw&M-IY,Xd;+');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
